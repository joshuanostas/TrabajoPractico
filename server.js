const express = require("express");
const http = require('http');
const app = express();
const server = http.createServer(app);
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;

const DATABASE_NAME = 'productosdb';
const MONGO_URL = `mongodb://josh:10xsiempre@ds153700.mlab.com:53700/productosdb`;

/*const DATABASE_NAME = 'ProductosDB';
const MONGO_URL = `mongodb://localhost:27017`;*/

let db = null;
let collection = null;
let collectionPedidos = null;

const timeout = 120000;

app.set('port', process.env.PORT || 3000);
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));

async function onGetRoot(req, res) {
    const cursor = await collectionPedidos.find()
                                   .project({ id:1, client: 1, state: 1, date: 1, total: 1 });
    const array = await cursor.toArray();
    const ids = array.map( o => o.id);
    const clientes = array.map( o => o.client);
    const estados = array.map(o => o.state);
    const fechas = array.map(o => o.date);
    const total = array.map(o => o.total);

    res.render("index", {ids, clientes, estados, fechas, total});
}

async function onGetNew(req,res) {
    const cursor = await collection.find()
                                   .project({ name: 1 });
    const array = await cursor.toArray();
    const products = array.map( o => o.name );

    let id =  '_' + Math.random().toString(36).substr(2, 9);
    
    res.render("new", {products , id});
}

async function onLookupOrder(req, res) {
    const id = req.query._id;
    const query = { id: id };
    const order = await collectionPedidos.findOne(query);
    res.render('view', { order });
}

async function onEdit(req, res) {
    const id = req.query._id;
    const query = { id: id };
    const order = await collectionPedidos.findOne(query);

    const cursor = await collection.find()
                                   .project({ name: 1 });
    const array = await cursor.toArray();
    const products = array.map( o => o.name );

    res.render('edit', { order, products });
}

async function onSave(req,res) {
    const client = req.body.cli;
    const ped = req.body.pedido;
    const list = converToArray(ped); 
    let state = req.body.estado;
    const total = req.body.total;

    const day = new Date().getDate();
    const month = new Date().getMonth()+1;
    const year = new Date().getFullYear();
    
    const date = day + '/' + month + '/' + year;

    const id = req.body.id;
    
    const query = { id };
    const newEntry = {id, client, state, date, total, list} ;
    const params = { upsert: true };
    const response = await collectionPedidos.update(query, newEntry, params);

    if(state === "solicitado"){
        setState(query);
    }
    res.redirect(`/`);
}

async function setState(query) {
    await sleep(timeout);
    let order = await collectionPedidos.findOneAndUpdate(query, {$set: {state: "completado"}});
}

app.get("/", onGetRoot);
app.get("/nuevo", onGetNew);
app.get("/lookup", onLookupOrder);
app.get("/edit", onEdit);
app.post("/save", onSave);

async function startServer() {
    const client = await MongoClient.connect(MONGO_URL, { useNewUrlParser: true });
    db = client.db(DATABASE_NAME);
    collection = db.collection('producto');
    collectionPedidos = db.collection('pedido');

    server.listen(app.get('port'), () => {console.log(`Server is up and running on port ${app.get('port')}`)});
}

startServer();

function converToArray(ped){
    let list = [];
    let palabra = "";
    let producto = [];
    for (var i = 0; i < ped.length; i++) {
        if(ped[i] != ','){
            palabra = palabra + ped[i];
            if(i === ped.length-1){
                producto.push(palabra);
                list.push(producto);
            }
        }else{
            if(isNaN(palabra)){
                producto.push(palabra);
            }else{
                producto.push(palabra);
                list.push(producto);
                producto = [];
            }
            palabra = "";
        }   
    }
    return list;
}

function sleep(time) {
    return new Promise(resolve => setTimeout(resolve,time));
}