class Entry {
    constructor(option, number, table, callback, callbackCount) {
        this.option = option;
        this.number = number;
        this.table = table;
        this.callback=callback;
        this.callbackCount=callbackCount;
        this.onDeleteEntry = this.onDeleteEntry.bind(this);
        this.setState = this.setState.bind(this);
        this.initializeUI(table);
        this.initializeHandlers();
    }

    initializeUI(table) {
        if(this.number && this.number>0){
            const row = table.insertRow(1);
            
            const product = row.insertCell(0);
            const cant = row.insertCell(1);
            const but = row.insertCell(2);
    
            product.textContent = this.option;
            cant.textContent = this.number;
            
            const button = document.createElement("button");

            button.classList.add("drop");
            button.innerText = "Eliminar";
            but.appendChild(button);
            this.callbackCount();
        }else{
            alert("Cantidad inválida");
        }
    }

    initializeHandlers(){
        const deleteEntry = document.querySelectorAll("table .drop");
        for (var i = 0; i < deleteEntry.length; i++) {
            deleteEntry[i].addEventListener("click", this.onDeleteEntry);            
        }

        const button = document.querySelectorAll(".but");
        for (var i = 0; i < button.length; i++) {
            button[i].addEventListener("click", this.setState);
        }
    }

    onDeleteEntry() {
        const toDelete = event.currentTarget;
        let cell = toDelete.parentNode;
        let row = cell.parentNode.remove();
        this.callbackCount();
        this.callback(this.table);
    }

    setState(){
        const estado = document.querySelector("#estado");
        if(event.target.dataset.id === "1"){
            estado.value = "borrador";
        }
        if(event.target.dataset.id === "2"){
            estado.value = "solicitado";
        }
    }
}