class App {
    constructor () {
        this.onAddEntry = this.onAddEntry.bind(this);
        this.updateList = this.updateList.bind(this);
        this.countProducts = this.countProducts.bind(this);
        this.initializeHandlers();
        this.pedidoCompleto = [];
    }

    initializeHandlers() {
        const addEntryButton = document.querySelector("table #add");
        addEntryButton.addEventListener("click", this.onAddEntry);

        const table = document.querySelector("table");
        new Entry("0", "5", table,this.updateList,this.countProducts);
        table.rows[1].remove();

        this.countProducts();
    }

    onAddEntry() {
        const select = document.querySelector("table #products");
        const number = document.querySelector("table #cantidad");
        const table = document.querySelector("table");

        const entry = new Entry(select.value, number.value, table,this.updateList,this.countProducts);

        this.updateList(table);
    }

    updateList(table){
        let aux = [];
        let send = ""; 
        this.pedidoCompleto = "";
        for (var i = 1; i < table.rows.length-1; i++) {
            for (var j = 0; j < 2; j++) {
                aux.push(table.rows[i].cells[j].textContent);
            }
        }
        aux.forEach(a=>{send += a + ',';});
        for (var i = 0; i < send.length-1; i++) {
            this.pedidoCompleto = this.pedidoCompleto + send[i];
        }
        console.log(this.pedidoCompleto);
        const addToList = document.querySelector("#pedido");
        addToList.value = this.pedidoCompleto;
    }

    countProducts(){
        const table = document.querySelector("table");
        const total = document.querySelector("#total");
        const input = document.querySelector("#inputTotal");
        total.textContent = table.rows.length - 2;
        input.value = table.rows.length - 2;
    }

}
