function showPost(id) {
    const pest = document.querySelector("#pest" + id);
    pest.classList.remove("hidden");

    const but = document.querySelectorAll(".tab button");
    but[id-1].classList.add("active");
}

function hidePost(id) {
    const pest = document.querySelector("#pest" + id);
    pest.classList.add("hidden");

    const but = document.querySelectorAll(".tab button");
    but[id-1].classList.remove("active");
}

function onClick(event) {
    if(event.target.dataset.id === "1")
    {
        hidePost(2);
        hidePost(3);
        showPost(1);
    }
    if(event.target.dataset.id === "2")
    {
        hidePost(1);
        hidePost(3);
        showPost(2);
    }
    if(event.target.dataset.id === "3")
    {
        hidePost(2);
        hidePost(1);
        showPost(3);
    }
}

function setupEventHandlers() {
    const button = document.querySelectorAll(".tab button");
    for (let index = 0; index < button.length; index++) {
        const but = button[index];
        but.addEventListener("click", onClick);
    }
}

function reloadPage() {
    const table = document.querySelector("#pest2 table");

    for (var i = 1; i < table.rows.length; i++) {
        if(table.rows[i].cells[1]){
            let timeout = 120001;
            function sleep(time) {
                return new Promise(resolve => setTimeout(resolve,time));
            }
            sleep(timeout).then(()=>{
                location.reload();
            })
        }
    }

}

reloadPage();
setupEventHandlers();